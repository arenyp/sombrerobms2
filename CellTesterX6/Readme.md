Cell tester board.

This board is designed to test 6 P18650 cells. It must be connected to a PC through usb and to a power source of 24V 3A.
It measures cells voltages and it can charge and discharge the cell stack to measure the charge discharge rate and the total capacity.
It can be used to do cell quality estimates and sorting.
It can balance the cells to a software value, meaning balancing severall stacks of 6 cells of the same type to the same values.






