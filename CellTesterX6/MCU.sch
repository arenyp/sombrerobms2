EESchema Schematic File Version 4
LIBS:CellTesterX6-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6900 3400 2    50   Output ~ 0
LED_RUN
Text HLabel 6900 3500 2    50   Output ~ 0
LED_ERR
Text HLabel 6900 3600 2    50   Output ~ 0
LED_BALANCE
Text HLabel 8350 3250 0    50   Output ~ 0
CS\
Text HLabel 8350 3350 0    50   Output ~ 0
SCLK
Text HLabel 8350 3450 0    50   Output ~ 0
SDI
Text HLabel 8350 3550 0    50   Input ~ 0
SDO
Text HLabel 8350 3700 0    50   Input ~ 0
ALERT
Text HLabel 8350 3850 0    50   Output ~ 0
PD\
Text HLabel 8350 3950 0    50   Output ~ 0
CNVST
Wire Wire Line
	2250 1700 2500 1700
Wire Wire Line
	2250 1800 2500 1800
Wire Wire Line
	2250 1900 2500 1900
Wire Wire Line
	2250 2000 2500 2000
Text Label 1150 3600 0    50   ~ 0
GND
Text Label 2500 1800 0    50   ~ 0
RX
Text Label 2500 1900 0    50   ~ 0
TX
Text Label 2500 2000 0    50   ~ 0
BOOT
$Comp
L Connector:Conn_01x05_Female J5
U 1 1 5C3AFAD1
P 2050 1900
F 0 "J5" H 1944 1475 50  0000 C CNN
F 1 "RST\\ BOOT TX RX GND" H 1944 1566 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x05_P1.27mm_Vertical" H 2050 1900 50  0001 C CNN
F 3 "~" H 2050 1900 50  0001 C CNN
	1    2050 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	2250 2100 2500 2100
Text Label 2500 2100 0    50   ~ 0
RST\
Text Notes 1850 1400 0    50   ~ 0
Serial bootloader pins
Wire Wire Line
	6150 2200 6150 1700
Wire Wire Line
	6150 1700 6050 1700
Wire Wire Line
	5950 2200 5950 1700
Connection ~ 5950 1700
Wire Wire Line
	5950 1700 5850 1700
Wire Wire Line
	6050 2200 6050 1700
Connection ~ 6050 1700
Wire Wire Line
	6050 1700 5950 1700
Wire Wire Line
	5850 5100 5850 5300
Wire Wire Line
	5850 5300 5500 5300
Wire Wire Line
	6050 5100 6050 5300
Wire Wire Line
	6050 5300 5950 5300
Connection ~ 5850 5300
Wire Wire Line
	5950 5100 5950 5300
Connection ~ 5950 5300
Wire Wire Line
	5950 5300 5850 5300
Text Label 5550 5300 0    50   ~ 0
GND
Text Label 5600 1700 0    50   ~ 0
3.3V
Text HLabel 5550 1700 0    50   Input ~ 0
VDD
Text HLabel 5500 5300 0    50   Input ~ 0
GND
Wire Wire Line
	6050 5300 6150 5300
Wire Wire Line
	6150 5300 6150 5100
Connection ~ 6050 5300
Text Label 6950 4400 0    50   ~ 0
RX
Text Label 6950 4300 0    50   ~ 0
TX
Text Label 5000 2400 0    50   ~ 0
RST\
Wire Wire Line
	6650 4500 6950 4500
Wire Wire Line
	6650 4600 6950 4600
Text Label 6950 4600 0    50   ~ 0
SPI1_MOSI
Text Label 6950 4500 0    50   ~ 0
SPI1_MISO
Wire Wire Line
	6650 4900 6950 4900
Wire Wire Line
	5450 3700 5000 3700
Text Label 5000 3700 0    50   ~ 0
SPI1_SCK
Text Label 6950 4900 0    50   ~ 0
SPI1_NSS
Text Label 8600 3350 0    50   ~ 0
SPI1_SCK
Text Label 8600 3550 0    50   ~ 0
SPI1_MISO
Text Label 8600 3450 0    50   ~ 0
SPI1_MOSI
Wire Wire Line
	8600 3350 8350 3350
Wire Wire Line
	8350 3450 8600 3450
Wire Wire Line
	8600 3550 8350 3550
Wire Wire Line
	8350 3250 8600 3250
Wire Wire Line
	8350 3700 8600 3700
Wire Wire Line
	8350 3850 8600 3850
Wire Wire Line
	8350 3950 8600 3950
Text Label 8600 3850 0    50   ~ 0
PDOWN\
Text Label 8600 3950 0    50   ~ 0
CONVSTART
Text Label 8600 3700 0    50   ~ 0
ALERT_IT
Wire Wire Line
	6650 3400 6900 3400
Wire Wire Line
	6900 3500 6650 3500
Wire Wire Line
	6650 3600 6900 3600
Text Label 5000 3400 0    50   ~ 0
ALERT_IT
Text Label 6900 3800 0    50   ~ 0
PDOWN\
Text Label 6900 3900 0    50   ~ 0
CONVSTART
Text Label 8600 3250 0    50   ~ 0
SPI1_NSS
Wire Wire Line
	6650 3800 6900 3800
Wire Wire Line
	6900 3900 6650 3900
Wire Wire Line
	5000 3400 5450 3400
$Comp
L Device:Crystal Y1
U 1 1 5BFD640E
P 4400 2700
F 0 "Y1" V 4354 2831 50  0000 L CNN
F 1 "Crystal" V 4445 2831 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_5032-2Pin_5.0x3.2mm_HandSoldering" H 4400 2700 50  0001 C CNN
F 3 "~" H 4400 2700 50  0001 C CNN
	1    4400 2700
	0    1    1    0   
$EndComp
$Comp
L Device:C C21
U 1 1 5C0DB7ED
P 4150 2950
F 0 "C21" V 3898 2950 50  0000 C CNN
F 1 "20pF" V 3989 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4188 2800 50  0001 C CNN
F 3 "~" H 4150 2950 50  0001 C CNN
	1    4150 2950
	0    1    1    0   
$EndComp
$Comp
L Device:C C20
U 1 1 5C0DB846
P 4150 2450
F 0 "C20" V 3898 2450 50  0000 C CNN
F 1 "20pF" V 3989 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4188 2300 50  0001 C CNN
F 3 "~" H 4150 2450 50  0001 C CNN
	1    4150 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 2550 4400 2450
Wire Wire Line
	4400 2450 4300 2450
Wire Wire Line
	4300 2950 4400 2950
Wire Wire Line
	4400 2950 4400 2850
Wire Wire Line
	4000 2450 3900 2450
Wire Wire Line
	3900 2450 3900 2950
Wire Wire Line
	3900 2950 4000 2950
Wire Wire Line
	3900 2950 3600 2950
Connection ~ 3900 2950
Text Label 3600 2950 0    50   ~ 0
GND
Wire Wire Line
	6650 4300 6950 4300
Wire Wire Line
	6950 4400 6650 4400
Text Label 5000 2800 0    50   ~ 0
BOOT
Wire Wire Line
	5450 2800 5000 2800
Wire Wire Line
	5450 2400 5000 2400
Wire Wire Line
	5450 2600 4900 2600
Wire Wire Line
	4900 2600 4900 2450
Wire Wire Line
	4900 2450 4400 2450
Connection ~ 4400 2450
Wire Wire Line
	5450 2700 4900 2700
Wire Wire Line
	4900 2700 4900 2950
Wire Wire Line
	4900 2950 4400 2950
Connection ~ 4400 2950
NoConn ~ 5450 3000
NoConn ~ 5450 3100
NoConn ~ 5450 3200
NoConn ~ 5450 3500
NoConn ~ 5450 3600
NoConn ~ 5450 3800
NoConn ~ 5450 3900
NoConn ~ 5450 4000
NoConn ~ 5450 4100
NoConn ~ 6650 4700
NoConn ~ 6650 4800
Wire Wire Line
	5850 2200 5850 1700
Connection ~ 5850 1700
Wire Wire Line
	5850 1700 5550 1700
Wire Wire Line
	6250 2200 6250 1700
Wire Wire Line
	6250 1700 6150 1700
Connection ~ 6150 1700
Wire Wire Line
	6650 3700 6900 3700
Text HLabel 6900 3700 2    50   Output ~ 0
LED_DISCHARGE
NoConn ~ 5450 4600
NoConn ~ 5450 4700
NoConn ~ 5450 4800
NoConn ~ 5450 4900
$Comp
L MCU_ST_STM32L4:STM32L431CBTx U6
U 1 1 5BFBC0FF
P 6050 3600
F 0 "U6" H 6450 2100 50  0000 C CNN
F 1 "STM32L431CBTx" H 6500 1950 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 5550 2200 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00257211.pdf" H 6050 3600 50  0001 C CNN
	1    6050 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4000 6900 4000
Text HLabel 6900 4000 2    50   Output ~ 0
CHARGE
$Comp
L Switch:SW_Push SW1
U 1 1 5CDCC903
P 3900 1650
F 0 "SW1" V 3854 1798 50  0000 L CNN
F 1 "SW_Push" V 3945 1798 50  0000 L CNN
F 2 "kicad-libraries:SW_06_SMD" H 3900 1850 50  0001 C CNN
F 3 "" H 3900 1850 50  0001 C CNN
	1    3900 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 2000 3600 2000
Text Label 3600 2000 0    50   ~ 0
GND
$Comp
L Device:R R2
U 1 1 5CDD22BD
P 3900 1100
F 0 "R2" H 3970 1146 50  0000 L CNN
F 1 "10K" H 3970 1055 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3830 1100 50  0001 C CNN
F 3 "~" H 3900 1100 50  0001 C CNN
	1    3900 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1850 3900 2000
Wire Wire Line
	3900 1450 3900 1350
Wire Wire Line
	3900 950  3900 850 
Wire Wire Line
	3900 850  3600 850 
Text Label 3600 850  0    50   ~ 0
VDD
Wire Wire Line
	3900 1350 4150 1350
Connection ~ 3900 1350
Wire Wire Line
	3900 1350 3900 1250
Text Label 4150 1350 0    50   ~ 0
RST\
$Sheet
S 1500 3250 1000 950 
U 5CE7A8CC
F0 "SerUsb" 50
F1 "SerUsb.sch" 50
F2 "TXD" I R 2500 3500 50 
F3 "RXD" I R 2500 3600 50 
F4 "GND" I L 1500 3600 50 
F5 "VCC" I L 1500 3500 50 
F6 "RTS" I R 2500 3800 50 
F7 "DTR" I R 2500 3900 50 
F8 "EN" I L 1500 3950 50 
$EndSheet
Wire Wire Line
	2500 3500 2750 3500
Wire Wire Line
	2500 3600 2750 3600
Wire Wire Line
	2500 3800 2750 3800
Text Label 2750 3500 0    50   ~ 0
RX
Text Label 2750 3600 0    50   ~ 0
TX
Text Label 2750 3800 0    50   ~ 0
BOOT
Wire Wire Line
	2500 3900 2750 3900
Text Label 2750 3900 0    50   ~ 0
RST\
Text Label 1150 3500 0    50   ~ 0
VDD
Wire Wire Line
	1150 3500 1500 3500
Wire Wire Line
	1500 3600 1150 3600
Text Label 2500 1700 0    50   ~ 0
GND
NoConn ~ 5450 4300
Wire Wire Line
	6650 4100 6900 4100
Text Label 6900 4100 0    50   ~ 0
USBENABLE
Text Label 950  3950 0    50   ~ 0
USBENABLE
Wire Wire Line
	1500 3950 950  3950
Wire Wire Line
	5450 4200 5000 4200
Text HLabel 5000 4200 0    50   Output ~ 0
FANENABLE
Text HLabel 4600 4400 0    50   Input ~ 0
SCL
Wire Wire Line
	4600 4400 5450 4400
Text Label 4900 4400 0    50   ~ 0
I2C2_SCL
Text HLabel 4600 4500 0    50   Input ~ 0
SDA
Wire Wire Line
	4600 4500 5450 4500
Text Label 4900 4500 0    50   ~ 0
I2C2_SDA
Wire Wire Line
	6650 4200 6900 4200
Text HLabel 6900 4200 2    50   Output ~ 0
DISCHARGE
$EndSCHEMATC
