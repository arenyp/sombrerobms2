EESchema Schematic File Version 4
LIBS:CellTesterX6-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Relay:G5Q-1 K1
U 1 1 5CF31E4D
P 5200 3450
AR Path="/5CF31B1F/5CF31E4D" Ref="K1"  Part="1" 
AR Path="/5CF5F211/5CF31E4D" Ref="K2"  Part="1" 
AR Path="/5C2EA202/5CFACD0A/5CF31E4D" Ref="K?"  Part="1" 
F 0 "K1" H 5630 3496 50  0000 L CNN
F 1 "G5Q-1" H 5630 3405 50  0000 L CNN
F 2 "lib:REL_PE014" H 5650 3400 50  0001 L CNN
F 3 "https://www.omron.com/ecb/products/pdf/en-g5q.pdf" H 5850 3300 50  0001 L CNN
	1    5200 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2750 5500 3150
Wire Wire Line
	5400 3750 5400 3850
NoConn ~ 5300 3150
$Comp
L Device:Q_NPN_BCE Q7
U 1 1 5CF31E57
P 4900 4200
AR Path="/5CF31B1F/5CF31E57" Ref="Q7"  Part="1" 
AR Path="/5CF5F211/5CF31E57" Ref="Q9"  Part="1" 
AR Path="/5C2EA202/5CFACD0A/5CF31E57" Ref="Q?"  Part="1" 
F 0 "Q7" H 5091 4246 50  0000 L CNN
F 1 "Q_NPN_BCE" H 5091 4155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5100 4300 50  0001 C CNN
F 3 "~" H 4900 4200 50  0001 C CNN
	1    4900 4200
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4148 D12
U 1 1 5CF31E5E
P 4400 3450
AR Path="/5CF31B1F/5CF31E5E" Ref="D12"  Part="1" 
AR Path="/5CF5F211/5CF31E5E" Ref="D15"  Part="1" 
AR Path="/5C2EA202/5CFACD0A/5CF31E5E" Ref="D?"  Part="1" 
F 0 "D12" V 4354 3529 50  0000 L CNN
F 1 "1N4148" V 4445 3529 50  0000 L CNN
F 2 "Diode_SMD:D_1206_3216Metric" H 4400 3275 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/1N4148_1N4448.pdf" H 4400 3450 50  0001 C CNN
	1    4400 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 3150 5000 3100
Wire Wire Line
	4400 3300 4400 3100
Connection ~ 4400 3100
Wire Wire Line
	4400 3100 5000 3100
Wire Wire Line
	5000 3750 5000 3850
Wire Wire Line
	5000 4400 5000 4550
Wire Wire Line
	5000 4550 5100 4550
Wire Wire Line
	5000 3850 4400 3850
Wire Wire Line
	4400 3850 4400 3600
Connection ~ 5000 3850
Wire Wire Line
	5000 3850 5000 4000
Wire Wire Line
	4700 4200 3900 4200
Wire Wire Line
	5400 3850 6350 3850
Wire Wire Line
	3900 3100 4400 3100
Text HLabel 3900 3100 0    50   Input ~ 0
VCC
Text HLabel 5100 4550 2    50   Input ~ 0
GND
Text HLabel 3900 4200 0    50   Input ~ 0
COMMAND
Text HLabel 6350 3850 2    50   Input ~ 0
OUT
Text HLabel 5250 2750 0    50   Input ~ 0
IN
Wire Wire Line
	5250 2750 5500 2750
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 5D55DA2E
P 4300 4700
F 0 "Q?" H 4491 4746 50  0000 L CNN
F 1 "Q_NPN_BEC" H 4491 4655 50  0000 L CNN
F 2 "" H 4500 4800 50  0001 C CNN
F 3 "~" H 4300 4700 50  0001 C CNN
	1    4300 4700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
