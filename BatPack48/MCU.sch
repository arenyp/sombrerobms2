EESchema Schematic File Version 4
LIBS:BatPack48-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2600 3750 0    50   Input ~ 0
RX
Text HLabel 2600 3850 0    50   Output ~ 0
TX
Text HLabel 6100 4200 0    50   Output ~ 0
LED_RUN
Text HLabel 6100 4100 0    50   Output ~ 0
LED_ERR
Text HLabel 6100 4000 0    50   Output ~ 0
LED_BALANCE
Text HLabel 10000 2800 0    50   Output ~ 0
CS\
Text HLabel 10000 2900 0    50   Output ~ 0
SCLK
Text HLabel 10000 3000 0    50   Output ~ 0
SDI
Text HLabel 10000 3100 0    50   Input ~ 0
SDO
Text HLabel 10000 3250 0    50   Input ~ 0
ALERT
Text HLabel 10000 3400 0    50   Output ~ 0
PD\
Text HLabel 10000 3500 0    50   Output ~ 0
CNVST
Text HLabel 2600 3500 0    50   Input ~ 0
BOOT
Text HLabel 2600 3600 0    50   Input ~ 0
RESET\
Text Label 6650 5600 0    50   ~ 0
GND
Text Label 6550 1400 0    50   ~ 0
3.3V
Text HLabel 6500 1400 0    50   Input ~ 0
VDD
Text HLabel 6600 5600 0    50   Input ~ 0
GND
Text Label 3050 3750 0    50   ~ 0
RX
Text Label 3050 3850 0    50   ~ 0
TX
Text Label 7850 3500 0    50   ~ 0
RX
Text Label 7850 3400 0    50   ~ 0
TX
Wire Wire Line
	2600 3500 3050 3500
Wire Wire Line
	2600 3600 3050 3600
Text Label 3050 3500 0    50   ~ 0
BOOT
Text Label 3050 3600 0    50   ~ 0
RST\
Text Label 5900 1900 0    50   ~ 0
RST\
Wire Wire Line
	7550 3800 7850 3800
Wire Wire Line
	7550 3900 7850 3900
Text Label 7850 3900 0    50   ~ 0
SPI1_MOSI
Text Label 7850 3800 0    50   ~ 0
SPI1_MISO
Wire Wire Line
	7550 3600 7850 3600
Text Label 7850 3700 0    50   ~ 0
SPI1_SCK
Text Label 7850 3600 0    50   ~ 0
SPI1_NSS
Text Label 10250 2900 0    50   ~ 0
SPI1_SCK
Text Label 10250 3100 0    50   ~ 0
SPI1_MISO
Text Label 10250 3000 0    50   ~ 0
SPI1_MOSI
Wire Wire Line
	10250 2900 10000 2900
Wire Wire Line
	10000 3000 10250 3000
Wire Wire Line
	10250 3100 10000 3100
Wire Wire Line
	10000 2800 10250 2800
Wire Wire Line
	10000 3250 10250 3250
Wire Wire Line
	10000 3400 10250 3400
Wire Wire Line
	10000 3500 10250 3500
Text Label 10250 3400 0    50   ~ 0
PDOWN\
Text Label 10250 3500 0    50   ~ 0
CONVSTART
Text Label 10250 3250 0    50   ~ 0
ALERT_IT
Wire Wire Line
	6350 4200 6100 4200
Wire Wire Line
	6100 4100 6350 4100
Wire Wire Line
	6350 4000 6100 4000
Text Label 7850 3200 0    50   ~ 0
ALERT_IT
Text Label 7850 4000 0    50   ~ 0
PDOWN\
Text Label 7850 3300 0    50   ~ 0
CONVSTART
Text Label 10250 2800 0    50   ~ 0
SPI1_NSS
$Comp
L Device:Crystal Y1
U 1 1 5BFD640E
P 5300 2400
F 0 "Y1" V 5254 2531 50  0000 L CNN
F 1 "Crystal" V 5345 2531 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_5032-2Pin_5.0x3.2mm_HandSoldering" H 5300 2400 50  0001 C CNN
F 3 "~" H 5300 2400 50  0001 C CNN
	1    5300 2400
	0    1    1    0   
$EndComp
$Comp
L Device:C C21
U 1 1 5C0DB7ED
P 5050 2650
F 0 "C21" V 4798 2650 50  0000 C CNN
F 1 "20pF" V 4889 2650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5088 2500 50  0001 C CNN
F 3 "~" H 5050 2650 50  0001 C CNN
	1    5050 2650
	0    1    1    0   
$EndComp
$Comp
L Device:C C20
U 1 1 5C0DB846
P 5050 2150
F 0 "C20" V 4798 2150 50  0000 C CNN
F 1 "20pF" V 4889 2150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5088 2000 50  0001 C CNN
F 3 "~" H 5050 2150 50  0001 C CNN
	1    5050 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	5300 2250 5300 2150
Wire Wire Line
	5300 2150 5200 2150
Wire Wire Line
	5200 2650 5300 2650
Wire Wire Line
	5300 2650 5300 2550
Wire Wire Line
	4900 2150 4800 2150
Wire Wire Line
	4800 2150 4800 2650
Wire Wire Line
	4800 2650 4900 2650
Wire Wire Line
	4800 2650 4500 2650
Connection ~ 4800 2650
Text Label 4500 2650 0    50   ~ 0
GND
Wire Wire Line
	7550 3400 7850 3400
Wire Wire Line
	7850 3500 7550 3500
Text Label 5900 2100 0    50   ~ 0
BOOT
Wire Wire Line
	6350 2100 5900 2100
Wire Wire Line
	6350 1900 5900 1900
Wire Wire Line
	6350 2300 5800 2300
Wire Wire Line
	5800 2300 5800 2150
Wire Wire Line
	5800 2150 5300 2150
Connection ~ 5300 2150
Wire Wire Line
	6350 2400 5800 2400
Wire Wire Line
	5800 2400 5800 2650
Wire Wire Line
	5800 2650 5300 2650
Connection ~ 5300 2650
Wire Wire Line
	7050 5200 7050 5600
Wire Wire Line
	6600 5600 7050 5600
Wire Wire Line
	6850 1400 6850 1700
Wire Wire Line
	6500 1400 6850 1400
Wire Wire Line
	6850 1400 6950 1400
Wire Wire Line
	6950 1400 6950 1700
Connection ~ 6850 1400
Wire Wire Line
	7550 3700 7850 3700
Wire Wire Line
	7550 4000 7850 4000
Wire Wire Line
	7850 3300 7550 3300
Wire Wire Line
	7550 3200 7850 3200
$Comp
L MCU_ST_STM32F0:STM32F030C6Tx U?
U 1 1 5D5ECB0F
P 6950 3300
F 0 "U?" H 6950 1614 50  0000 C CNN
F 1 "STM32F030C6Tx" H 6950 1523 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 6450 1800 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00088500.pdf" H 6950 3300 50  0001 C CNN
	1    6950 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 5200 6950 5200
Connection ~ 7050 5200
Connection ~ 6950 5200
Wire Wire Line
	6950 5200 7050 5200
Wire Wire Line
	6950 1400 7050 1400
Wire Wire Line
	7150 1400 7150 1700
Connection ~ 6950 1400
Wire Wire Line
	7050 1700 7050 1400
Connection ~ 7050 1400
Wire Wire Line
	7050 1400 7150 1400
Wire Wire Line
	6850 4900 6850 5200
Wire Wire Line
	6950 4900 6950 5200
Wire Wire Line
	7050 4900 7050 5200
Text HLabel 5950 3200 0    50   Input ~ 0
ADR0
Text HLabel 5950 3300 0    50   Input ~ 0
ADR1
Text HLabel 5950 3400 0    50   Input ~ 0
ADR2
Text HLabel 5950 3500 0    50   Input ~ 0
ADR3
Wire Wire Line
	5950 3200 6350 3200
Wire Wire Line
	6350 3300 5950 3300
Wire Wire Line
	5950 3400 6350 3400
Wire Wire Line
	6350 3500 5950 3500
NoConn ~ 6350 2500
NoConn ~ 6350 2600
NoConn ~ 6350 2800
NoConn ~ 6350 2900
NoConn ~ 6350 3000
NoConn ~ 6350 3600
NoConn ~ 6350 3700
NoConn ~ 6350 3800
NoConn ~ 6350 3900
NoConn ~ 6350 4400
NoConn ~ 6350 4500
NoConn ~ 6350 4600
NoConn ~ 6350 4700
NoConn ~ 7550 4400
NoConn ~ 7550 4500
NoConn ~ 7550 4600
NoConn ~ 7550 4700
NoConn ~ 7550 4100
NoConn ~ 7550 4200
NoConn ~ 7550 4300
NoConn ~ 6350 4300
Wire Wire Line
	2600 3750 3050 3750
Wire Wire Line
	2600 3850 3050 3850
$EndSCHEMATC
