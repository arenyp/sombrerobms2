
## Discontinued unfinished project!
However I keep it here because, sometimes, I pick parts from it.

This project schematics are drawn with Kicad.

This is a fork of SombreroBMS which was to be used in golf carts and was never completed. It is repurposed to things like electric skateboards or bikes. However the dimension of the cells and their connexion are the only difference between a car battery pack and a skateboard battery pack.
SombreroBMS2 would work with 18650 cells.

It is designed in two pieces:
- the measurement and balancing board with a digital insulator and a CAN µcontroller powered from the main board
- the main controller board featuring a buzzer, a current sensor, a charge voltage sensor, a charge stop output to the charger, dc-dc converter, power button input, and MosFets to break the circuits.

With this design it is possible to couple two 25volts modules in parallel (for power) or to stakc them up (up to 50V) at relatively low cost. Using only 2 power wires and the CAN bus connection (the CAN µcontroller is powered from the main board). The modules can be in different places on the vehicle wich is a very important feature for a BMS.

The controller board is a fork of the DieBiems. I discovered DieBiems because SombreroBMS on GitHub was 'stared' by the DieBiems developer. I take the control part of it and add bluetooth in order to program the firmware wirelessly and dialog with the UC.
The voltage measurement and balancing is on can bus modules. The uCs are insulated from the measurement chips. It should be robust, extensible and it may not fry completely in case of an accident.






