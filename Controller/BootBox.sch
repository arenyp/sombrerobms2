EESchema Schematic File Version 4
LIBS:Controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5550 4050 0    50   ~ 0
BOOT0
$Comp
L Connector:TestPoint TP?
U 1 1 5C832839
P 6100 3950
AR Path="/5C832839" Ref="TP?"  Part="1" 
AR Path="/5C81147C/5C832839" Ref="TP13"  Part="1" 
F 0 "TP13" H 6158 4070 50  0000 L CNN
F 1 "TestPoint" H 6158 3979 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6300 3950 50  0001 C CNN
F 3 "~" H 6300 3950 50  0001 C CNN
	1    6100 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5C832840
P 6100 3000
AR Path="/5C832840" Ref="TP?"  Part="1" 
AR Path="/5C81147C/5C832840" Ref="TP11"  Part="1" 
F 0 "TP11" H 6158 3120 50  0000 L CNN
F 1 "TestPoint" H 6158 3029 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6300 3000 50  0001 C CNN
F 3 "~" H 6300 3000 50  0001 C CNN
	1    6100 3000
	1    0    0    -1  
$EndComp
Text HLabel 4550 3150 0    50   Input ~ 0
USB_TX
Text HLabel 4550 3050 0    50   Input ~ 0
BT_TX
Text HLabel 4550 3650 0    50   Input ~ 0
USB_DTR
Text HLabel 4550 3550 0    50   Input ~ 0
BT_CTS
Text HLabel 4550 4100 0    50   Input ~ 0
USB_RTS
Text HLabel 4550 4000 0    50   Input ~ 0
BT_RTS
Wire Wire Line
	4550 3050 4900 3050
Wire Wire Line
	4900 3150 4550 3150
Wire Wire Line
	4550 3550 4900 3550
Wire Wire Line
	4900 3650 4550 3650
Wire Wire Line
	4550 4000 4900 4000
Wire Wire Line
	4900 4100 4550 4100
Wire Wire Line
	5450 3100 6100 3100
Wire Wire Line
	5450 3600 6100 3600
Wire Wire Line
	5450 4050 6100 4050
Text Label 5550 3600 0    50   ~ 0
RESET\
Text HLabel 6400 3100 2    50   Input ~ 0
TX
Text HLabel 6400 3600 2    50   Input ~ 0
RESET\
Text HLabel 6400 4050 2    50   Input ~ 0
BOOT0
$Comp
L Connector:TestPoint TP?
U 1 1 5C834264
P 6100 3500
AR Path="/5C834264" Ref="TP?"  Part="1" 
AR Path="/5C81147C/5C834264" Ref="TP12"  Part="1" 
F 0 "TP12" H 6158 3620 50  0000 L CNN
F 1 "TestPoint" H 6158 3529 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6300 3500 50  0001 C CNN
F 3 "~" H 6300 3500 50  0001 C CNN
	1    6100 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3000 6100 3100
Connection ~ 6100 3100
Wire Wire Line
	6100 3100 6400 3100
Wire Wire Line
	6100 3500 6100 3600
Connection ~ 6100 3600
Wire Wire Line
	6100 3600 6400 3600
Wire Wire Line
	6100 3950 6100 4050
Connection ~ 6100 4050
Wire Wire Line
	6100 4050 6400 4050
$Comp
L 74xGxx:74AHC1G32 U13
U 1 1 5C96EE5E
P 5200 3600
F 0 "U13" H 5175 3867 50  0000 C CNN
F 1 "74AHC1G32" H 5175 3776 50  0000 C CNN
F 2 "Package_SO:Texas_R-PDSO-N5" H 5200 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5200 3600 50  0001 C CNN
	1    5200 3600
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G02 U11
U 1 1 5C8331BA
P 5200 4050
F 0 "U11" H 5175 4317 50  0000 C CNN
F 1 "74AHC1G02" H 5175 4226 50  0000 C CNN
F 2 "Package_SO:Texas_R-PDSO-N5" H 5200 4050 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5200 4050 50  0001 C CNN
	1    5200 4050
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G32 U10
U 1 1 5C96EF93
P 5200 3100
F 0 "U10" H 5175 3367 50  0000 C CNN
F 1 "74AHC1G32" H 5175 3276 50  0000 C CNN
F 2 "Package_SO:Texas_R-PDSO-N5" H 5200 3100 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5200 3100 50  0001 C CNN
	1    5200 3100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
