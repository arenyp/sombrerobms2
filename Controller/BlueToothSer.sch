EESchema Schematic File Version 4
LIBS:Controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5000 4500 0    50   Input ~ 0
GND
Text HLabel 4800 2550 0    50   Input ~ 0
VDD
Text HLabel 3850 3350 0    50   Input ~ 0
RX
Text HLabel 3850 3450 0    50   Input ~ 0
TX
$Comp
L RF_Bluetooth:RN4871 U8
U 1 1 5C7751BC
P 5350 3650
F 0 "U8" H 5350 4428 50  0000 C CNN
F 1 "RN4871" H 5350 4337 50  0000 C CNN
F 2 "RF_Module:Microchip_RN4871" H 5350 2950 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/50002489A.pdf" H 4850 4200 50  0001 C CNN
	1    5350 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3350 4650 3350
Wire Wire Line
	4650 3450 3850 3450
Wire Wire Line
	5000 4500 5250 4500
Wire Wire Line
	5450 4500 5450 4250
Wire Wire Line
	5250 4250 5250 4500
Connection ~ 5250 4500
Wire Wire Line
	5250 4500 5450 4500
Wire Wire Line
	5350 3050 5350 2550
Wire Wire Line
	5350 2550 4800 2550
$Comp
L Device:LED_ALT D16
U 1 1 5C7759EA
P 6500 3850
F 0 "D16" H 6492 3595 50  0000 C CNN
F 1 "LED_ALT" H 6492 3686 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6500 3850 50  0001 C CNN
F 3 "~" H 6500 3850 50  0001 C CNN
	1    6500 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3850 6050 3850
$Comp
L Device:R R42
U 1 1 5C775A8A
P 6900 3850
F 0 "R42" V 7107 3850 50  0000 C CNN
F 1 "4.7K" V 7016 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6830 3850 50  0001 C CNN
F 3 "~" H 6900 3850 50  0001 C CNN
	1    6900 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 3850 6750 3850
Text Label 5200 2550 0    50   ~ 0
VDD
Text Label 5050 4500 0    50   ~ 0
GND
Wire Wire Line
	7050 3850 7450 3850
Text Label 7450 3850 0    50   ~ 0
VDD
Wire Wire Line
	4650 3550 3850 3550
Text HLabel 3850 3550 0    50   Input ~ 0
RTS
NoConn ~ 6050 3350
Wire Wire Line
	4650 3850 3850 3850
Text HLabel 3850 3850 0    50   Input ~ 0
CTS
$Comp
L Power_Supervisor:CAT811ZTBI-GT3 U7
U 1 1 5C98D474
P 2300 3750
F 0 "U7" H 2741 3796 50  0000 L CNN
F 1 "CAT811ZTBI-GT3" H 2741 3705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-143" H 2400 3450 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/CAT811-D.PDF" H 1950 3050 50  0001 C CNN
	1    2300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 3750 4650 3750
Wire Wire Line
	2300 4050 2300 4200
Wire Wire Line
	2300 4200 2550 4200
Wire Wire Line
	2300 3450 2300 3250
Wire Wire Line
	2300 3250 2550 3250
Text Label 2550 3250 0    50   ~ 0
VDD
Text Label 2550 4200 0    50   ~ 0
GND
NoConn ~ 6050 3450
NoConn ~ 6050 3550
NoConn ~ 6050 3650
NoConn ~ 6050 3750
NoConn ~ 6050 3950
NoConn ~ 4650 3950
NoConn ~ 1900 3750
Text Label 3400 3750 0    50   ~ 0
BlueToothReset
$EndSCHEMATC
